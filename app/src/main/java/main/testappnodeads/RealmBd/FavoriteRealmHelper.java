package main.testappnodeads.RealmBd;

import android.content.Context;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import main.testappnodeads.Fragments.Favorite.Model.FavoriteListModel;

public class FavoriteRealmHelper {

    private Realm realm;
    private RealmResults<FavoritePersonObject> realmResults;
    private static FavoriteRealmHelper instance = null;

    private FavoriteRealmHelper(Context context) {

        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(config);
        realmResults = realm.where(FavoritePersonObject.class).findAll();
        if (realmResults != null) {
            realmResults.addChangeListener(element -> {
                RealmPresenter realmPresenter = RealmPresenter.getInstance();
                realmPresenter.updateFavoriteList();
            });
        }
    }

    public static synchronized FavoriteRealmHelper getInstance(Context context) {
        if (instance == null) {
            instance = new FavoriteRealmHelper(context);
        }
        return instance;
    }

    private int getCount() {
        return realmResults.size();
    }

    public void remove(String id) {
        realm.executeTransaction(realm ->   realmResults.where().equalTo("id", id).findAll().deleteAllFromRealm());
    }

    public void add(String firstname, String lastname, String placeOfWork,
                    String position, String linkPDF, String id) {
        FavoritePersonObject favoritePersonObject = new FavoritePersonObject();

        favoritePersonObject.setFirstname(firstname);
        favoritePersonObject.setLastname(lastname);
        favoritePersonObject.setPlaceOfWork(placeOfWork);
        favoritePersonObject.setPosition(position);
        favoritePersonObject.setLinkPDF(linkPDF);
        favoritePersonObject.setId(id);

        realm.beginTransaction();
        realm.insert(favoritePersonObject);
        realm.commitTransaction();
    }

    public ArrayList<FavoriteListModel> show() {
        String firstname, lastname, placeOfWork, position, linkPDF, id;
        ArrayList<FavoriteListModel> data = new ArrayList<>();

        realmResults.sort("id");

        if (realmResults.size() > 0) {
            for (int i = 0; i < realmResults.size(); i++) {
                firstname = realmResults.get(i).getFirstname();
                lastname = realmResults.get(i).getLastname();
                placeOfWork = realmResults.get(i).getPlaceOfWork();
                position = realmResults.get(i).getPosition();
                linkPDF = realmResults.get(i).getLinkPDF();
                id = realmResults.get(i).getId();
                data.add(new FavoriteListModel(firstname, lastname, placeOfWork, position, linkPDF, id));
            }
        }

        return data;

    }
}
