package main.testappnodeads.RealmBd;

import main.testappnodeads.Fragments.Favorite.Presenter.FavoriteListPresenter;
import main.testappnodeads.Fragments.Persons.Presenter.PersonListPresenter;

public class RealmPresenter {

    private static RealmPresenter instance;

    private RealmPresenter() {

    }

    public static synchronized RealmPresenter getInstance() {
        if (instance == null){
            instance = new RealmPresenter();
        }
        return instance;
    }

    public void updateFavoriteList(){
        FavoriteListPresenter favoriteListPresenter = FavoriteListPresenter.getInstance();
        PersonListPresenter personListPresenter = PersonListPresenter.getInstance();
        favoriteListPresenter.updateFavoriteList();
        personListPresenter.updateFavoriteList();
    }
}
