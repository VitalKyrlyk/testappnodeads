package main.testappnodeads.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PersonList {

    public class Items {

        String firstname;
        String lastname;
        String placeOfWork;
        String position;
        String linkPDF;
        String id;

        public String getFirstname() {
            return firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public String getPlaceOfWork() {
            return placeOfWork;
        }

        public String getPosition() {
            return position;
        }

        public String getLinkPDF() {
            return linkPDF;
        }

        public String getId() {
            return id;
        }
    }

    @SerializedName("items")
    private List<Items> items;

    public List<Items> getItems() {
        return items;
    }


}
