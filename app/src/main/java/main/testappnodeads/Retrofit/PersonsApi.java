package main.testappnodeads.Retrofit;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class PersonsApi {

    private static final String BASE_URL = "https://public-api.nazk.gov.ua";
    private static Retrofit retrofit = null;
    private static ApiInterface apiInterface = null;

    public interface ApiInterface {
        @GET("/v1/declaration/")
        Call<PersonList> getData(@Query("q") String resourceName);
    }

    private static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static ApiInterface interfaceCreate(){
        if (apiInterface == null) {
            apiInterface = PersonsApi.getClient().create(PersonsApi.ApiInterface.class);
        }
        return apiInterface;
    }

}
