package main.testappnodeads.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import main.testappnodeads.Activity.Presenter.MainActivityPresenter;
import main.testappnodeads.Activity.View.MainView;
import main.testappnodeads.Fragments.Favorite.FavoriteListFragment;
import main.testappnodeads.Fragments.Persons.PersonsListFragment;
import main.testappnodeads.Fragments.Persons.rvAdapter.PersonListAdapter;
import main.testappnodeads.R;
import main.testappnodeads.Retrofit.PersonList;

public class MainActivity extends AppCompatActivity implements MainView {

    MainActivityPresenter mainActivityPresenter;
    FragmentPagerItemAdapter fragmentPagerItemAdapter;
    PersonListAdapter adapter;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.viewPagerTabLayout)
    SmartTabLayout smartTabLayout;

    @BindView(R.id.searchET)
    EditText searchEt;

    @OnClick({R.id.searchBtn})
    public void Search(){
        mainActivityPresenter.tryGetPersons(searchEt.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
        .add("Результати пошуку", PersonsListFragment.class)
        .add("Обрані", FavoriteListFragment.class)
        .create());
        viewPager.setAdapter(fragmentPagerItemAdapter);
        smartTabLayout.setViewPager(viewPager);
        mainActivityPresenter = new MainActivityPresenter(this);

    }

    public void getAdapterFromFragment(PersonListAdapter adapter){
        this.adapter = adapter;
    }

    @Override
    public void getListPersons(List<PersonList.Items> personLists) {
        Fragment fragment = fragmentPagerItemAdapter.getItem(viewPager.getCurrentItem());
        if (fragment instanceof PersonsListFragment) {
            ((PersonsListFragment) fragment).getListPersons(personLists, adapter);
        }
    }

    @Override
    public void showNotCorrectlyNameError() {
        searchEt.setError(getString(R.string.NotCorrectlyNameError));
    }

    @Override
    public void showIsSuccessfulError() {
        Toast.makeText(this, R.string.isSuccessfulError, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showConnectionError() {
        Toast.makeText(this, R.string.connectionError, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyResponseBodyError() {
        Toast.makeText(this, R.string.emptyResponseBodyError, Toast.LENGTH_LONG).show();
    }
}
