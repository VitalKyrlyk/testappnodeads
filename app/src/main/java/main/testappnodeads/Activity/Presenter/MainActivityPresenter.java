package main.testappnodeads.Activity.Presenter;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Objects;

import main.testappnodeads.Activity.View.MainView;
import main.testappnodeads.Retrofit.PersonList;
import main.testappnodeads.Retrofit.PersonsApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityPresenter {

    private PersonsApi.ApiInterface apiInterface;
    private MainView mView;

    public MainActivityPresenter(MainView mainView) {
       apiInterface = PersonsApi.interfaceCreate();
       mView = mainView;
    }

    public void tryGetPersons(String name){

        if (!name.isEmpty()){

            Call<PersonList> listCall = apiInterface.getData(name);
            listCall.enqueue(new Callback<PersonList>() {

                @Override
                public void onResponse(@NonNull Call<PersonList> call, @NonNull Response<PersonList> response) {

                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (Objects.requireNonNull(response.body()).getItems() == null) {
                                mView.showEmptyResponseBodyError();
                            } else {
                                List<PersonList.Items> items = Objects.requireNonNull(response.body()).getItems();
                                mView.getListPersons(items);
                            }
                        } else {
                            mView.showIsSuccessfulError();
                        }
                    } else {
                        mView.showIsSuccessfulError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PersonList> call, @NonNull Throwable t) {
                    mView.showConnectionError();
                }
            });
        } else {
            mView.showNotCorrectlyNameError();
        }
    }

}
