package main.testappnodeads.Activity.View;

import java.util.List;

import main.testappnodeads.Retrofit.PersonList;

public interface MainView {

    void showNotCorrectlyNameError();

    void showIsSuccessfulError();

    void showConnectionError();

    void showEmptyResponseBodyError();

    void getListPersons(List<PersonList.Items> personLists);

}
