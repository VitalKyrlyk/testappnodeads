package main.testappnodeads.Fragments;

public interface BaseFragmentInterface {

    void updateFavoriteList();

    void showDeleteFromFavoriteMessage();

    void startActivityPdf(String pdfSource);
}
