package main.testappnodeads.Fragments.Favorite;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import main.testappnodeads.Fragments.Favorite.Presenter.FavoriteListPresenter;
import main.testappnodeads.Fragments.Favorite.View.FavoriteListView;
import main.testappnodeads.Fragments.Favorite.rvAdapter.FavoriteListAdapter;
import main.testappnodeads.R;


public class FavoriteListFragment extends Fragment implements FavoriteListView {

    View view;
    FavoriteListPresenter favoriteListPresenter;
    FavoriteListAdapter adapter;

    @BindView(R.id.rvFavorite)
    RecyclerView recyclerView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite_persons_list, null);
        ButterKnife.bind(this, view);

        favoriteListPresenter = FavoriteListPresenter.getInstance();
        favoriteListPresenter.getView(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        adapter = new FavoriteListAdapter(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        return view;

    }

    @Override
    public void updateFavoriteList() {
        adapter.updateList();
    }

    @Override
    public void showDeleteFromFavoriteMessage() {
        Toast.makeText(getContext(), "Видалено з обраних!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startActivityPdf(String pdfSource) {
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(pdfSource));
        startActivity(intent);
    }
}
