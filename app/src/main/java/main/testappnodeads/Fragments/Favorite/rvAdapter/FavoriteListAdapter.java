package main.testappnodeads.Fragments.Favorite.rvAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import main.testappnodeads.Fragments.Favorite.Model.FavoriteListModel;
import main.testappnodeads.Fragments.Favorite.Presenter.FavoriteListPresenter;
import main.testappnodeads.R;
import main.testappnodeads.RealmBd.FavoriteRealmHelper;

public class FavoriteListAdapter extends RecyclerView.Adapter<FavoriteListAdapter.ViewHolder> {

    private FavoriteRealmHelper favoriteRealmHelper;
    private List<FavoriteListModel> favoriteListModels;
    private FavoriteListPresenter favoriteListPresenter;

    public FavoriteListAdapter(Context context) {
        favoriteRealmHelper = FavoriteRealmHelper.getInstance(context);
        favoriteListModels = favoriteRealmHelper.show();
        favoriteListPresenter = FavoriteListPresenter.getInstance();
    }

    @NonNull
    @Override
    public FavoriteListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        return new FavoriteListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteListAdapter.ViewHolder holder, int position) {

        if (favoriteListModels != null){
            String firstName = favoriteListModels.get(position).getFirstname();
            String lastname = favoriteListModels.get(position).getLastname();
            String placeOfWork = favoriteListModels.get(position).getPlaceOfWork();
            String posada = favoriteListModels.get(position).getPosition();
            String pdfSource = favoriteListModels.get(position).getLinkPDF();
            String id = favoriteListModels.get(position).getId();

            holder.firstName.setText(firstName);
            holder.secName.setText(lastname);
            holder.placeOfWork.setText(placeOfWork);
            holder.position.setText(posada);
            holder.imageButtonFavorite.setImageResource(R.drawable.btn_star_big_on);

            holder.imageButtonPdf.setOnClickListener(view -> {
                favoriteListPresenter.startActivityPdf(pdfSource);
            });

            holder.imageButtonFavorite.setOnClickListener(view -> {
                    favoriteRealmHelper.remove(id);
                    favoriteListPresenter.showDeleteFromFavoriteMessage();
            });

        }

    }

    @Override
    public int getItemCount() {
        if (favoriteListModels == null){
            return 0;
        } else {
            return favoriteListModels.size();
        }
    }

    public void updateList(){
        favoriteListModels = favoriteRealmHelper.show();
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView firstName;
        TextView secName;
        TextView placeOfWork;
        TextView position;

        ImageButton imageButtonPdf;
        ImageButton imageButtonFavorite;

        private ViewHolder(View itemView) {
            super(itemView);

            firstName = itemView.findViewById(R.id.firstName);
            secName = itemView.findViewById(R.id.secName);
            placeOfWork = itemView.findViewById(R.id.placeOfWork);
            position = itemView.findViewById(R.id.position);

            imageButtonPdf = itemView.findViewById(R.id.imageButtonPdf);
            imageButtonFavorite = itemView.findViewById(R.id.imageButtonFavorite);

        }
    }

}
