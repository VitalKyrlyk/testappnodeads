package main.testappnodeads.Fragments.Favorite.Presenter;

import main.testappnodeads.Fragments.Favorite.View.FavoriteListView;

public class FavoriteListPresenter {

    private static FavoriteListPresenter instance;
    private FavoriteListView favoriteListView;

    private FavoriteListPresenter() {

    }

    public static synchronized FavoriteListPresenter getInstance() {
        if (instance == null){
            instance = new FavoriteListPresenter();
        }
        return instance;
    }

    public void getView (FavoriteListView view){
        this.favoriteListView = view;
    }

    public void updateFavoriteList(){
        favoriteListView.updateFavoriteList();
    }

    public void showDeleteFromFavoriteMessage(){
        favoriteListView.showDeleteFromFavoriteMessage();
    }

    public void startActivityPdf(String pdfSource){
        favoriteListView.startActivityPdf(pdfSource);
    }

}
