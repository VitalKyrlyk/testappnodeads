package main.testappnodeads.Fragments.Persons.rvAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import main.testappnodeads.Fragments.Favorite.Model.FavoriteListModel;
import main.testappnodeads.Fragments.Persons.Presenter.PersonListPresenter;
import main.testappnodeads.R;
import main.testappnodeads.RealmBd.FavoriteRealmHelper;
import main.testappnodeads.Retrofit.PersonList;

public class PersonListAdapter extends RecyclerView.Adapter<PersonListAdapter.ViewHolder> {

    private List<PersonList.Items> personLists;
    private FavoriteRealmHelper favoriteRealmHelper;
    private List<FavoriteListModel> favoriteListModels;
    private PersonListPresenter personListPresenter;

    public PersonListAdapter(Context context) {
        favoriteRealmHelper = FavoriteRealmHelper.getInstance(context);
        favoriteListModels = favoriteRealmHelper.show();
        personListPresenter = PersonListPresenter.getInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (personLists != null){
            String firstName = "І'мя по батькові: " + personLists.get(position).getFirstname();
            String lastname = "Призвіще: " + personLists.get(position).getLastname();
            String placeOfWork = "Місце роботи: " + personLists.get(position).getPlaceOfWork();
            String posada = "Посада: " + personLists.get(position).getPosition();
            String pdfSource = personLists.get(position).getLinkPDF();
            String id = personLists.get(position).getId();

            holder.firstName.setText(firstName);
            holder.secName.setText(lastname);
            holder.placeOfWork.setText(placeOfWork);
            holder.position.setText(posada);

            holder.imageButtonFavorite.setImageResource(R.drawable.btn_star_big_off);
            holder.imageButtonFavorite.setSelected(false);
            if (favoriteListModels.size() > 0){
                for (int i = 0; i < favoriteListModels.size(); i++){
                    if (favoriteListModels.get(i).getId().equals(id)){
                        holder.imageButtonFavorite.setImageResource(R.drawable.btn_star_big_on);
                        holder.imageButtonFavorite.setSelected(true);
                    }
                }
            }

            holder.imageButtonPdf.setOnClickListener(view -> {
                personListPresenter.startActivityPdf(pdfSource);
            });

            holder.imageButtonFavorite.setOnClickListener(view -> {
                if (!holder.imageButtonFavorite.isSelected()){
                    favoriteRealmHelper.add(firstName, lastname, placeOfWork, posada, pdfSource, id);
                    personListPresenter.showAddToFavoriteMessage();
                } else {
                    favoriteRealmHelper.remove(id);
                    personListPresenter.showDeleteFromFavoriteMessage();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (personLists == null){
            return 0;
        } else {
            return personLists.size();
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setList(List<PersonList.Items> list){
        this.personLists = list;
        notifyDataSetChanged();
    }

    public void updateList(){
        favoriteListModels = favoriteRealmHelper.show();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView firstName;
        TextView secName;
        TextView placeOfWork;
        TextView position;

        ImageButton imageButtonPdf;
        ImageButton imageButtonFavorite;

        private ViewHolder(View itemView) {
            super(itemView);

            firstName = itemView.findViewById(R.id.firstName);
            secName = itemView.findViewById(R.id.secName);
            placeOfWork = itemView.findViewById(R.id.placeOfWork);
            position = itemView.findViewById(R.id.position);

            imageButtonPdf = itemView.findViewById(R.id.imageButtonPdf);
            imageButtonFavorite = itemView.findViewById(R.id.imageButtonFavorite);

        }
    }

}
