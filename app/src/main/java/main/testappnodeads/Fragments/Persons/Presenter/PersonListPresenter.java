package main.testappnodeads.Fragments.Persons.Presenter;

import main.testappnodeads.Fragments.Persons.View.PersonListView;

public class PersonListPresenter {

    private static PersonListPresenter instance;
    private PersonListView personListView;

    private PersonListPresenter() {

    }

    public static synchronized PersonListPresenter getInstance() {
        if (instance == null){
            instance = new PersonListPresenter();
        }
        return instance;
    }

    public void getView (PersonListView view){
        this.personListView = view;
    }

    public void updateFavoriteList(){
        personListView.updateFavoriteList();
    }

    public void showAddToFavoriteMessage(){
        personListView.showAddToFavoriteMessage();
    }

    public void showDeleteFromFavoriteMessage(){
        personListView.showDeleteFromFavoriteMessage();
    }

    public void startActivityPdf(String pdfSource){
        personListView.startActivityPdf(pdfSource);
    }
}
