package main.testappnodeads.Fragments.Persons;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import main.testappnodeads.Activity.MainActivity;
import main.testappnodeads.Fragments.Persons.Presenter.PersonListPresenter;
import main.testappnodeads.Fragments.Persons.View.PersonListView;
import main.testappnodeads.Fragments.Persons.rvAdapter.PersonListAdapter;
import main.testappnodeads.R;
import main.testappnodeads.Retrofit.PersonList;

public class PersonsListFragment extends Fragment implements PersonListView {

    View view;
    PersonListAdapter adapter;
    MainActivity mainActivity;
    PersonListPresenter personListPresenter;

    @BindView(R.id.rvPerson)
    RecyclerView recyclerView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (MainActivity) activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_persons_list, null);
        ButterKnife.bind(this, view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        adapter = new PersonListAdapter(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        getAdapter(adapter);
        personListPresenter = PersonListPresenter.getInstance();
        personListPresenter.getView(this);

        return view;
    }

    public void getAdapter(PersonListAdapter personListAdapter){
        mainActivity.getAdapterFromFragment(personListAdapter);
    }

    public void getListPersons(List<PersonList.Items> personLists, PersonListAdapter listAdapter) {
        listAdapter.setList(personLists);
    }

    @Override
    public void updateFavoriteList() {
        adapter.updateList();
    }

    @Override
    public void showAddToFavoriteMessage() {
        Toast.makeText(getContext(), "Додано до обраних!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showDeleteFromFavoriteMessage() {
        Toast.makeText(getContext(), "Видалено з обраних!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startActivityPdf(String pdfSource) {
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(pdfSource));
        startActivity(intent);
    }
}
