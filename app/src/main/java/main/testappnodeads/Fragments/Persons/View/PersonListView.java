package main.testappnodeads.Fragments.Persons.View;

import main.testappnodeads.Fragments.BaseFragmentInterface;

public interface PersonListView extends BaseFragmentInterface {

    void showAddToFavoriteMessage();
}
